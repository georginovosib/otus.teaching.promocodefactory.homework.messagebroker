﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Common;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class EventConsumer : IConsumer<GivePromoCodeRequestAdmin>
    {
        private readonly IReceivingFromPartnerService _receivingFromPartnerService;

        public EventConsumer(IReceivingFromPartnerService receivingFromPartnerService)
        {
            _receivingFromPartnerService = receivingFromPartnerService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeRequestAdmin> context)
        {
            await _receivingFromPartnerService.AddPromocodesAsync(context.Message);
        }

       
    }
}
