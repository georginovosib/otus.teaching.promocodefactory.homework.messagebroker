﻿using System;


namespace Otus.Teaching.Pcf.Common
{
    public class GivePromoCodeRequestAdmin
    {
        public Guid? PartnerId { get; set; }
    }
}

