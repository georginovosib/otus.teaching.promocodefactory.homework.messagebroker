﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Common;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class ReceivingFromPartnerService : IReceivingFromPartnerService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public ReceivingFromPartnerService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task AddPromocodesAsync(GivePromoCodeRequestAdmin message)
        {
            var employee = await _employeeRepository.GetByIdAsync(message.PartnerId.Value);

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
